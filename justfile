#For Powershell uncomment below
set shell := ["powershell.exe", "-c"]

#install cargo-audit, cargo-tarpaulin, add rust components clippy, rustfmt

release-debug:
    cargo build

release-build:
    cargo build --release

format-code:
    cargo fmt -- --check

audit-code:
    cargo audit

lint-code:
    cargo clippy -- -D warnings

test-code:
    cargo test
    cargo tarpaulin --ignore-tests

ci-project:
    just format-code
    just lint-code
    just audit-code
    just test-code