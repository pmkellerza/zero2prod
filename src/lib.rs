use axum::response::IntoResponse;
use axum::{http, routing::get, Router};
use tokio::net::TcpListener;

pub async fn run(listener: TcpListener) -> Result<(), std::io::Error> {
    let router = Router::new().route("/health_check", get(health_check));
    axum::serve(listener, router).await
}

async fn health_check() -> impl IntoResponse {
    http::StatusCode::OK
}
